/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.finalproject1;

/**
 *
 * @author Arthit
 */
public class TestProject {
    public static void main(String[] args) {
        Human extra = new Human ("Extra"); //สร้าง Object จากคลาส Humen ชื่อว่า Extra และ กำหนดชื่อเข้าไปใน Constuctor
        extra.setMoney(0); //set จำนวนเงิน ให้กับ Object extra เป็น 0
        extra.setYugiCard(30); //set จำนวนการ์ดยูกิให้กับ Object extra เป็น 30
        extra.show(); //แสดงรายละเอียดทั้งหมดของ Object extra
        
        Human yaisa = new Human("Yaisa"); //สร้าง Object จากคลาส Humen ชื่อว่า Yaisa และ กำหนดชื่อเข้าไปใน Constuctor
        yaisa.setMoney(2000); //set จำนวนเงิน ให้กับ Object yaisa เป็น 2000
        yaisa.setYugiCard(0); //set จำนวนการ์ดยูกิให้กับ Object yaisa เป็น 0
        yaisa.show(); //แสดงรายละเอียดทั้งหมดของ Object yaisa
        
        Human teacher = new Human("Teacher"); //สร้าง Object จากคลาส Humen ชื่อว่า Teacher และ กำหนดชื่อเข้าไปใน Constuctor
        teacher.setMoney(1000); //set จำนวนเงิน ให้กับ Object teacher เป็น 1000
        teacher.setYugiCard(0); //set จำนวนการ์ดยูกิให้กับ Object teacher เป็น 0
        teacher.show(); //แสดงรายละเอียดทั้งหมดของ Object teacher
         Enter();
        
        extra.earnMoney(30); //นายต้าได้รับเงินจากยาย 30 บาท (ใช้ Object extra เรียกใช้ method earnMoney เพื่อเพิ่มค่า Attribute money ให้กับ Object extra ใน class Human)
        System.out.println(extra.getName()+" have money "+extra.getMoney()+" Bath."); //แสดงผลเงินต้า (ให้ Object extra เรียกใช้ method getter ในคลาส Human เพื่อแสดงข้อมูล ชื่อ และเงิน )
        
        yaisa.useMoney(30);  //ยายสาได้ให้เงินกับนายต้าทำให้เงินลดลง 30 บาท (ใช้ Object yaisa เรียกใช้ method useMoney เพื่อลดค่า Attribute money ให้กับ Object yaisa ใน class Human)
        System.out.println(yaisa.getName()+" have money "+yaisa.getMoney()+" Bath."); //แสดงผลเงินยายสา (ให้ Object yaisa เรียกใช้ method getter ในคลาส Human เพื่อแสดงข้อมูล ชื่อ และเงิน )
        
        extra.useMoney(10);  //นายต้าใช้เงินซื้อข้าวเช้า 10 บาท (ใช้ Object extra เรียกใช้ method useMoney เพื่อลดค่า Attribute money ให้กับ Object extra ใน class Human)
        System.out.println(extra.getName()+" have money "+extra.getMoney()+" Bath."); //แสดงผลเงินนายต้า (ให้ Object extra เรียกใช้ method getter ในคลาส Human เพื่อแสดงข้อมูล ชื่อ และเงิน )
        
        extra.addCard(50); //นายต้าเล่นการ์ดยูกิกับเพื่อนจนได้ของเพื่อนมา 50 ใบ (ใช้ Object extra เรียกใช้ method addCard เพื่อเพิ่มค่า Attribute yugiCard ให้กับ Object extra ใน class Human)
        extra.showCard(); //แสดงการ์ด (ใช้ Object extra เรียกใช้ method showCard เพื่อแสดงจำนวนการ์ด)
        
        extra.seizeCard(20); // นายต้าโดนครูยึดการ์ด 20 ใบ (ใช้ Object extra เรียกใช้ method seizeCard เพื่อลดค่า Attribute yugiCard ให้กับ Object extra ใน class Human)
        extra.showCard(); //แสดงการ์ด (ใช้ Object extra เรียกใช้ method showCard เพื่อแสดงจำนวนการ์ด)
        
        teacher.addCard(20); //ครูได้การ์ดยูกิกับนายต้า 20 ใบ  (ใช้ Object teacher เรียกใช้ method addCard เพื่อเพิ่มค่า Attribute yugiCard ให้กับ Object teacher ใน class Human)
        teacher.showCard();  //แสดงการ์ด (ใช้ Object teacher เรียกใช้ method showCard เพื่อแสดงจำนวนการ์ด)
        
        extra.seizeCard(60); //นายต้าโดนยายยึดการ์ดจนหมด (ใช้ Object extra เรียกใช้ method seizeCard เพื่อลดค่า Attribute yugiCard ให้กับ Object extra ใน class Human)
        extra.useMoney(20); // นายต้าโดนยายหักเงินค่าขนมทั้งหมด (ใช้ Object extra เรียกใช้ useMoney เพื่อลดค่า Attribute Money ให้กับ Object extra ใน class Human)
        extra.showCard(); //แสดงการ์ด (ใช้ Object extra เรียกใช้ method showCard เพื่อแสดงจำนวนการ์ด)
        extra.showMoney(); //แสดงจำนวนเงิน (ใช้ Object extra เรียกใช้ method showMoney เพื่อแสดงจำนวนเงิน)
        
        yaisa.addCard(60); //ยายสาได้การ์ดจากนายต้า 60 ใบ (ใช้ Object yaisa เรียกใช้ method addCard เพื่อเพิ่มค่า Attribute yugiCard ให้กับ Object yaisa ใน class Human)
        yaisa.earnMoney(20); //ยายสาได้เิงนจากนายต้า 20 บาท (ใช้ Object yaisa เรียกใช้ method earnMoney เพื่อเพิ่มค่า Attribute money ให้กับ Object yaisa ใน class Human)
        yaisa.showCard(); //แสดงการ์ด (ใช้ Object yaisa เรียกใช้ method showCard เพื่อแสดงจำนวนการ์ด)
        yaisa.showMoney();//แสดงจำนวนเงิน (ใช้ Object yaisa เรียกใช้ method showMoney เพื่อแสดงจำนวนเงิน)
        
        Enter();
        System.out.println("Show Status Everyone."); //แสดงข้อมูล Object ทั้งหมดได้แก่ ชื่อ จำนวนเงิน จำนวนการ์ด
        extra.show(); 
        yaisa.show();
        teacher.show();
    }

    private static void Enter() {
        System.out.println("---------------------------------------------");
        System.out.println("");
    }
    
   
    
    
}
