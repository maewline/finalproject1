/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.finalproject1;

/**
 *
 * @author Arthit
 */
public class Human {
    private String name;
    private int money;
    private int yugiCard;
    //ค่า Attribute ในคลาส Human มี Modifier เป็น private นั่นหมายความว่า จะนำไปใช้ในงาน Class อื่นไม่ได้ จำเป็นต้องเข้าถึงผ่าน method getter เพื่อ return ค่า และ setter เพื่อกำหนดค่า
    
    public Human (String name){  //สร้าง Constructor โดยรับ Parameter เป็นชื่อของ Object
        this.name = name;
    }

    public int getMoney() {  //สร้าง getter สำหรับค่า money เพื่อนำค่าเงินไปใช้งาน
        return money;
    }
    
    public String getName() {
        return name;
    }

    public int getYugiCard() {  //สร้าง getter สำหรับค่า yugiCard เพื่อนำจำนวนการ์ดยูกิไปใช้งาน
        return yugiCard;
    }

    public void setMoney(int money) {  //สร้าง setter เพื่อ set ค่า เงินให้กับ Object และรับ parameter มาเป็น int
         this.money = money;
    }

    public void setYugiCard(int yugiCard) {  //สร้าง setter เพื่อ set ค่า จำนวนการ์ดยูกิให้กับ Object รับ parameter มาเป็น int
        this.yugiCard=yugiCard;
    }
    
    public void useMoney (int money){  //สร้าง method การใช้เงิน เพื่อให้ Object สามารถใช้เงินได้ ซึ่งรับค่า parameter มาเป็น int
        if(this.money<money || this.money<=0){  // ถ้าเงินของ Object ที่มีอยู่ น้อยกว่า จำนวน parameter ส่งเข้ามา หรือ เงินของ Object ตอนนั้นน้อยกว่าหรือเท่ากับ 0 ให้แสดงข้อความว่า มีเงินไม่เพียงพอ
            System.out.println("Money not enough");
        }else{  // ถ้าไม่ตรงกับเงื่อนไขด้านบน ให้นำค่า parameter ที่รับเข้ามา มาลบกับ Attribute money ได้เลย
            this.money-=money;
        }
    }
    
    public void earnMoney(int money){ //สร้าง method ได้รับเงิน เพื่อทำการเพิ่มค่าเงินให้กับ Object ซึ่งรับค่า parameter มาเป็น int
        if(money>0){    // ถ้าค่า parameter มากกว่า 0 สามารถ นำมาบวกกับ Attribute money ได้เลย
            this.money+=money;
        }else{ // ถ้าไม่ตรงเงื่อนไขบน ให้แสดงข้อความเตือนออกไป เพราะเงินที่รับมาต้องมากกว่า 0 ถึงจะ make sense
            System.out.println("Money must more than 0 !!");
        }
    }
    
    public void show(){ // Method นี้จะแสดงสถานะทั้งหมดของ Object ได้แก่ ชื่อ จำนวนเงิน จำนวนการ์ด
        System.out.println("Hi! I am "+this.name+". Now I have money "+this.money+" Bath."+" and I have Yugi Card "+this.yugiCard+" Cards.");
    }
    
   public void showCard(){ // Method นี้จะแสดงจำนวนการ์ดของ Object เพียงอย่างเดียว
       System.out.println("Hi! I am "+this.name+". I have Yugi Card "+this.yugiCard+". Cards.");
   }
   
   public void showMoney(){ //Method นี้จะแสดงจำนวนเงินของ Object เพียงอย่างเดียว
       System.out.println("Hi! I am "+this.name+". I have money "+this.money+" Bath.");
   }
    
    public void seizeCard(int card){ //Method นี้จำทำหน้าที่ยึดการ์ดจาก Object ซึ่งรับค่า parameter มาเป็น int ถ้า...
        if(card>0 && this.yugiCard>0){  //parameter มากกว่า 0 และ Object มี การ์ดมากกว่า 0 ใบ สามารถยึดได้ตาม parameter เลย
            this.yugiCard-=card;
        }else if(card>0 && this.yugiCard<=0){  //ถ้า parameter มากกว่า 0 และ Object ไม่มีการ์ดเลย จะให้ส่งข้อความเตือนไปว่าไม่มีการ์ดให้ยึด และไม่ทำการยึด
            System.out.println("No cards for seize.");
        }else if (card>this.yugiCard){ //ถ้า parameter มากกกว่า การ์ดที่มีอยู่ใน Object ให้ข้อความเตือนว่า มีการ์ดไม่เพียงพอ และไม่ทำการยึด
            System.out.println("not enough cards to seize.");
        }      
    }
    
    public void addCard(int card){  //Method นี้สำหรับเพิ่มการ์ดยูกิให้กับ Object ซึ่งรับค่า parameter มาเป็น int โดยมีเงื่อนไขคือ...
        if(card>0){ //ถ้าค่าของ parameter มากกว่า 0 สามารถนำ parameter บวกเข้ากับ Attribute card ได้เลย
            this.yugiCard+=card;
        }else{ // ถ้าไม่ตรงเงื่อนไขห้ส่งข้อความเตือนว่าจำนวนการ์ดต้องมากกว่า 0
            System.out.println("Yugi card you must more than 0!!");
        }
     
    }
     
}
